**I chose Ryanair and AerLingus to test my scripts.**

# File Structure

A folder structure has been created to reflect the airline websites chosen.
Under the parent (airline) folders there are src and resources folders.
The src folder contains the script to parse data.
The resources folder contains the test plans.

# Assumptions

* These scripts were developed using Google Chrome and have not been tested in other browsers. Please use in other browsers with caution.
* These scripts are designed to work with desktop versions of the airline websites. As a result they can be used (in their current state) when the browser window has a minimum width of 1200px.
* Flight data will not be consistently formatted or structured. It will be read directly as presented in the airline web page. Examples include:
  * Departure and arrival times (generally airport local)
  * Origin and destination format (code, location, etc.)
* Script can’t account for flights that exceed a day
* Flight duration will not be calculated
* Console logging is used


# How to Use/Test

### Ryanair

1. Navigate to [Ryanair](https://www.ryanair.com/ie/en/travel-updates) using a browser
2. Enter some data in the form to search for flight data
3. Open a Javascript console in the browser
4. Copy and paste the contents of `ryanair/src/website_one_script.js` into the console

### AerLingus

1. Navigate to [AerLingus](https://www.aerlingus.com/html/trip-mgmt.html) using a browser
2. Enter some data in the form to search for flight data
3. Open a Javascript console in the browser
4. Copy and paste the contents of `aerlingus/src/website_two_script.js` into the console

# How to Update Script for Other Similar Airline Web Pages

Change the data in the settings structure
Some helpful information about this structure:
  * `apiURL` is the URL destination for the data parsed from the web page
  * It's intended that `getFlightDOMRoots` allows you to select multiple root DOM elements, e.g. all elements in a list of flights.
  * Each data field, e.g. flight number, has a `getWithRoot` in the settings structure. Change this function to return data relative to a DOM element selected by `getFlightDOMRoots` document, window, etc.
