/*
 * Required data is passed to Flight (data model) function via arguments.
 * As the task didn't request flight status data I haven't included fields
 * for things like actual departure time and estimated arrival time.
 * However, this structure could easily be extended to store such data.
 * Without validation (see validateFlightNumber) and calculated data (see getAirline)
 * this function doesn't do much but I think it's important to build for extendability
 * and this function allows that.
 */
var Flight = function(flightNumber, origin, destination, localDepartureTime, localArrivalTime) {

  var _flightNumber;
  var _origin;
  var _destination;
  var _localDepartureTime;
  var _localArrivalTime

  setFlightNumber(flightNumber);
  setOrigin(origin);
  setDestination(destination);
  setLocalDepartureTime(localDepartureTime);
  setLocalArrivalTime(localArrivalTime);

  /*
   * Setters allows us to encapsulate the data. They also make it easier to add
   * features like formatting and data validation, e.g. validating the flight
   * number format is two alphabetic characters followed by integers.
   */

  function setFlightNumber(flightNumber) {
    _flightNumber = flightNumber;
  }

  /*
  function validateFlightNumber(flightNumber) {
    // Call from setFlightNumber and use a regex comparison to validate the flight number is in the correct format
  }
  */

  /*
  function getAirline() {
    //Parse _flightNumber and use a map or API to derive airline, e.g. FR = Ryanair
  }
  */

  /*
   * Ideally airport origin and destination would be in or could be mapped to
   * aiport codes but I'm going to work with a more general format for the purposes
   * of this task
   */

  /*
  function setOriginAirportCode(originAirportCode) {
    _originAirportCode = originAirportCode;
  }
  */

  /*
  function setDestinationAirportCode(destinationAirportCode) {
    _destinationAirportCode = destinationAirportCode;
  }
  */

  function setOrigin(origin) {
    _origin = origin;
  }

  function setDestination(destination) {
    _destination = destination;
  }

  /*
   * It would be nice to make and ensure the date/time fields are Date objects.
   * Then we could work out things like flight duration, i.e. difference in UTC times.
   * I don't believe this is in scope for this task but it would be a nice enhancement.
   * This would also make it easier to account for flights that arrive on a different local date.
   */

  /*
  function setLocalDepartureDateTime(localDepartureDateTime) {
    _localDepartureDateTime = localDepartureDateTime;
  }
  */

  /*
  function setLocalArrivalDateTime(localArrivalDateTime) {
    _localArrivalDateTime = localArrivalDateTime;
  }
  */

  function setLocalDepartureTime(localDepartureTime) {
    _localDepartureTime = localDepartureTime;
  }

  function setLocalArrivalTime(localArrivalTime) {
    _localArrivalTime = localArrivalTime;
  }

  return {
    flightNumber: _flightNumber,
    origin: _origin,
    destination: _destination,
    localDepartureTime: _localDepartureTime,
    localArrivalTime: _localArrivalTime
  }

}

function parseDOM(webPageData) {
  var flights = [];
  var flighDOMRoots = webPageData.getFlightDOMRoots();
  Array.prototype.slice.call(flighDOMRoots).forEach(flightDOMRoot => {
    flights.push({
      flightNumber: webPageData.flightNumber.getWithRoot(flightDOMRoot),
      origin: webPageData.origin.getWithRoot(flightDOMRoot),
      destination: webPageData.destination.getWithRoot(flightDOMRoot),
      departureTime: webPageData.departureTime.getWithRoot(flightDOMRoot),
      arrivalTime: webPageData.arrivalTime.getWithRoot(flightDOMRoot)
    })
  })
  return flights;
}

function sendData(url, data) {
  var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
       // The task doesn't describe specify if there's an API response
       if(xhr.readyState === 4 && xhr.status === 200) {
         console.trace("Sent data to: " + url);
         console.debug("Sent data: " + data);
         console.info("Response: " + xhr.responseText);
       }
       else {
         console.error("Error sending data: " + data + " to: " + url);
       }
    }
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(data);
}

// Change data in this function/structure depending on requirements
var settings = {
  apiURL: "api.capturedata.ie",
  domParsing: {
    getFlightDOMRoots: function() {
      var roots = document.querySelectorAll("#flightStatusForm div tbody tr");
      if (roots.length === 0) {
        console.error("No HTML DOM flight roots found");
      }
      return roots;
    },
    flightNumber: {
      getWithRoot: function(flightDOMRoot) {
        var flightNumberDOM = flightDOMRoot.querySelector("th span");
        if (flightNumberDOM === null) {
          console.warn("No flight number found for flight: " + flightDOMRoot);
        }
        return flightNumberDOM.innerText;
      }
    },
    origin: {
      getWithRoot: function(flightDOMRoot) {
        var originDOM = document.querySelector("#flightStatusForm div table caption h2 span span:nth-child(1)");
        if (originDOM === null) {
          console.warn("No origin found for flight: " + flightDOMRoot);
        }
        return originDOM.innerText;
      }
    },
    destination: {
      getWithRoot: function(flightDOMRoot) {
        var destinationDOM = document.querySelector("#flightStatusForm div table caption h2 span b span:nth-child(1)");
        if (destinationDOM === null) {
          console.warn("No destination found for flight: " + flightDOMRoot);
        }
        return destinationDOM.innerText;
      }
    },
    departureTime: {
      getWithRoot: function(flightDOMRoot) {
        var departureTimeDOM = flightDOMRoot.querySelector("td:nth-of-type(1) div div:nth-of-type(2)");
        if (departureTimeDOM === null) {
          console.warn("No departure time found for flight: " + flightDOMRoot);
        }
        return departureTimeDOM.innerText;
      }
    },
    arrivalTime: {
      getWithRoot: function(flightDOMRoot) {
        var arrivalTimeDOM = flightDOMRoot.querySelector("td:nth-of-type(3) div div:nth-of-type(2)");
        if (arrivalTimeDOM === null) {
          console.warn("No arrival time found for flight: " + flightDOMRoot);
        }
        return arrivalTimeDOM.innerText;
      }
    }
  }
}

function main() {
  var domData = parseDOM(settings.domParsing);
  var flights = domData.map(flightDomData => new Flight(
    flightDomData.flightNumber,
    flightDomData.origin,
    flightDomData.destination,
    flightDomData.departureTime,
    flightDomData.arrivalTime
  ));
  sendData(settings.apiURL, flights);
};

main();
